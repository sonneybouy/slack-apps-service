import { Controller, Get, Param, Post, Request } from '@nestjs/common';
import { IBlocks } from './poll/model/blocks/IBlocks';
import { SlackBlockService } from './poll/service/slack-block.service';
import { PollService } from './poll/service/poll.service';
import { IPoll } from './poll/model/poll/IPoll';
import { IPayload } from './poll/model/payload/IPayload';
import axios from 'axios';
import {  ObjectId } from 'mongodb';
import { Poll } from './poll/model/poll/poll.schema';


@Controller()
export class AppController {
  private pollMap: Map<string, IBlocks>;

  constructor(private readonly pollService: PollService,
              private readonly slackBlockService: SlackBlockService) {
    this.pollMap = new Map<string, IBlocks>();
  }

  @Get('/')
  hello() {
    return "Hello World"
  }

  @Post('create')
  async createPoll(@Request() req): Promise<IBlocks> {
    const poll: IPoll = await this.pollService.generatePoll(req.body.text);
    const blocks: IBlocks = this.slackBlockService.createBlocks(poll);

    console.log(JSON.stringify(blocks));

    console.log(poll.id);
    return blocks;
  }

  @Post('activity')
  async amendPoll(@Request() req): Promise<Boolean> {
    console.log("Request Received");
    const payload: IPayload = JSON.parse(req.body.payload); //contains the action value === pollId
    const pollId: string = payload.actions[0].value;
    console.log("PollId is:",  pollId);
    const poll: Poll = await this.pollService.findOne(new ObjectId(pollId));
    const completePoll: IPoll = {
      id:pollId,
      question: poll.question,
      options: poll.options
    }

    const updatedPoll: IPoll = await this.pollService.update(completePoll, payload);
    const updatedBlocks = this.slackBlockService.createBlocks(updatedPoll)
    axios.post(payload.response_url, updatedBlocks);
    return true;

  }


}
