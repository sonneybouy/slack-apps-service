import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { PollService } from './poll/service/poll.service';
import { SlackBlockService } from './poll/service/slack-block.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [ PollService, SlackBlockService ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });


});
