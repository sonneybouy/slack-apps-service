import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PollService } from './poll/service/poll.service';
import { SlackBlockService } from './poll/service/slack-block.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Poll, PollSchema } from './poll/model/poll/poll.schema';
import { PollRepository } from './poll/service/poll.repository';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://sonneypatel:9ht6IWazMNofrvUc@slackapp.ssta9.gcp.mongodb.net/blockrepo?retryWrites=true&w=majority'),
    MongooseModule.forFeature([{ name: Poll.name, schema: PollSchema }])
  ],
  controllers: [AppController],
  providers: [ SlackBlockService, PollService, PollRepository],
})
export class AppModule {}
