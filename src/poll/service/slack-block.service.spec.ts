import { SlackBlockService } from './slack-block.service';
import { Test, TestingModule } from '@nestjs/testing';


describe("SlackBlockService", () => {

  let slackBlockService : SlackBlockService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
        providers: [SlackBlockService],
      },
    ).compile();

    slackBlockService = app.get<SlackBlockService>(SlackBlockService);
  });

});