import { Injectable } from '@nestjs/common';
import { IOption, IPoll } from '../model/poll/IPoll';
import { IBlocks } from '../model/blocks/IBlocks';
import { IAccessory, ISection, IText } from '../model/blocks/ISection';
import { Types } from '../model/blocks/Types';
import { IPayload } from '../model/payload/IPayload';
import {ObjectId} from 'mongodb'
import { IDivider } from '../model/blocks/IDivider';
import { IContext, IElement } from '../model/blocks/IContext';


@Injectable()
export class SlackBlockService {
  createBlocks(poll: IPoll) : IBlocks {
    let blocks = []
    const header = this.createHeader(poll.question);
    blocks.push(header);

    let divider : IDivider = {
      "type": Types.DIVIDER
    }
    blocks.push(divider);


    for (let i = 0; i < poll.options.length; i ++ ) {
      blocks.push(this.createOptions(poll.options[i].nomination, poll.id, i+1));
      blocks.push(this.createContext(poll.options[i]));
    }

    return {
      blocks: blocks,
      response_type : "in_channel"
    }
  }

  private createContext(option: IOption) :IContext {
    const voterString :string = option.voters.join(" ");
    const voteString : string = option.votes == 1 ? " vote" : " votes";
    const voterElement : IElement = {
      "type" : Types.PLAIN_TEXT,
      "text" : voterString +  ' ' + option.votes + voteString
    }

    return {
      "type": Types.CONTEXT,
      "elements":[voterElement]
    }
  }

  private createHeader(inputString : string) : ISection {
    const text = {
      type: Types.MARKDOWN,
      text: inputString
    };

    return <ISection>{
      type: Types.SECTION,
      text: text
    }
  }

  private createOptions(pollOption: string, actionValue: ObjectId, element: Number) : ISection {

    const text = {
      type: Types.MARKDOWN,
      text: pollOption
    }
    const accessory: IAccessory = {
      type: Types.BUTTON,
      text: {
        type: Types.PLAIN_TEXT,
        emoji: true,
        text: element.toString()
      },
      value: actionValue
    };

    return <ISection> {
      type:Types.SECTION,
      text: text,
      accessory : accessory
    }
  }



  updateBlocks(blocks: IBlocks, payload: IPayload) {
    console.log(JSON.stringify(blocks));
    blocks.blocks.forEach(block => {
      if(block.accessory && block.accessory.text.text === payload.actions[0].text.text) {
        if(!block.text.text.includes(payload.user.username)) {
          //add vote here

          block.text.text = block.text.text + " " + payload.user.username;
        } else {
        //  remove the vote.
          block.text.text = block.text.text.replace(payload.user.username, '');
        }
      }
    });

    return blocks;

  }
}