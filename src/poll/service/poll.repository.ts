import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Poll } from '../model/poll/poll.schema';
import { Model} from 'mongoose';
import {ObjectId} from 'mongodb';
import { IPoll } from '../model/poll/IPoll';


@Injectable()
export class PollRepository {
  constructor(@InjectModel(Poll.name) private pollModel: Model<Poll>) {}

  save(poll) {
    const pollModel = new this.pollModel(poll);
    pollModel._id = poll.id;
    return pollModel.save()
  }

  async findOne(id:ObjectId): Promise<IPoll> {
    return this.pollModel.findById(id);
  }

  async updatePoll(iPoll:IPoll) : Promise<IPoll> {
    const createdPoll = new this.pollModel(iPoll);
    createdPoll._id = iPoll.id;
    //TODO This was deprecated
    await this.pollModel.findByIdAndUpdate(iPoll.id, createdPoll);
    return {
      id: iPoll.id,
      question: iPoll.question,
      options: iPoll.options
    }
  }
}