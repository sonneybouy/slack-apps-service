import { PollService } from './poll.service';
import { Test, TestingModule } from '@nestjs/testing';
import { IPoll } from '../model/poll/IPoll';
import { PollRepository } from './poll.repository';
import { IPayload } from '../model/payload/IPayload';
import { Types } from '../model/blocks/Types';


class MockPollRepository {

  save(poll:IPoll) :IPoll{
    return poll;
  }

  updatePoll(iPoll: IPoll) :IPoll{
    return iPoll;
  }

}

describe('PollService', () => {

  let pollService: PollService;

  beforeEach(async () => {
    const pollRepositoryProvider = {
      provide: PollRepository,
      useClass: MockPollRepository
    }
    const app: TestingModule = await Test.createTestingModule({
        providers: [PollService, pollRepositoryProvider],
      },
    ).compile();

    pollService = app.get<PollService>(PollService);
  });

  test('accepts String input and creates a poll Object', () => {
    const input : string = '"Question A" "Option A" "Option B"';
    const output: IPoll = pollService.generatePoll(input);
    expect(output.question).toEqual("Question A");
    expect(output.options).toHaveLength(2);
  });

  test('adds votes to Poll With Payload', async () => {
    const testIPoll: IPoll = {
      id: undefined,
      options: [{
        nomination: "pizza", votes: 0, voters: []
      }],
      question: 'What would you like for dinner?'
    }

    const testPayload: IPayload = {
      actions: [{
        action_id: 'actionId',
        block_id: 'blockId',
        value: 'value',
        type: Types.PLAIN_TEXT,
        action_ts: 'actionTs',
        text: {
          type: Types.PLAIN_TEXT,
          text: "1",
          emoji: null
        }
      }],
      api_app_id: '',
      channel: undefined,
      response_url: '',
      token: '',
      trigger_id: '',
      type: '',
      user: {
        id: 'id',
        name: 'name',
        team_id: 'teamId',
        username: "SonneyPatel"
      },
    }

    const resultPoll: IPoll = await pollService.update(testIPoll, testPayload);

    expect(resultPoll.options[0].votes).toEqual(1);
    expect(resultPoll.options[0].voters[0]).toEqual("SonneyPatel");


  });

  test('removes votes to Poll With Payload', async () => {
    const testIPoll: IPoll = {
      id: undefined,
      options: [{
        nomination: "pizza", votes: 1, voters: ["SonneyPatel"]
      }],
      question: 'What would you like for dinner?'
    }

    const testPayload: IPayload = {
      actions: [{
        action_id: 'actionId',
        block_id: 'blockId',
        value: 'value',
        type: Types.PLAIN_TEXT,
        action_ts: 'actionTs',
        text: {
          type: Types.PLAIN_TEXT,
          text: "1",
          emoji: null
        }
      }],
      api_app_id: '',
      channel: undefined,
      response_url: '',
      token: '',
      trigger_id: '',
      type: '',
      user: {
        id: 'id',
        name: 'name',
        team_id: 'teamId',
        username: "SonneyPatel"
      },
    }

    const resultPoll: IPoll = await pollService.update(testIPoll, testPayload);

    expect(resultPoll.options[0].votes).toEqual(0);
    expect(resultPoll.options[0].voters.length).toEqual(0);


  });
});