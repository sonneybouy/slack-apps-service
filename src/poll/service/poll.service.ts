import { Injectable } from '@nestjs/common';
import { IPoll } from '../model/poll/IPoll';
import {ObjectId} from 'mongodb';
import { Poll } from '../model/poll/poll.schema';
import { IPayload } from '../model/payload/IPayload';
import { PollRepository } from './poll.repository';



@Injectable()
export class PollService {

  constructor(private readonly pollRepository: PollRepository) {}

  generatePoll(input: string): IPoll {
    const inputString = input.split('”').join('"');
    const finalInput = inputString.split('“').join('"');
    console.log("New input string = " ,finalInput);
    const inputArray: string[] = finalInput.split('" "');
    const trimmedInputArray: string[] = [];
    inputArray.forEach(value => {
      trimmedInputArray.push(value.split('"').join(''));
    });
    const question: string = trimmedInputArray[0];
    const nominations: string[] = trimmedInputArray.slice(1);

    let options = []
    for(let nom of nominations) {
      let option = {
        "nomination" : nom,
        "votes" : 0,
        "voters" :[]
      }
      options.push(option);
    }

    const poll :IPoll = {
      id: new ObjectId(),
      question,
       options,
    }
    return this.pollRepository.save(poll);
  }

  async findOne(id:ObjectId) :Promise<Poll> {
    return this.pollRepository.findOne(id);
  }

  async update(iPoll:IPoll, payload:IPayload): Promise<IPoll> {
    console.log(JSON.stringify(iPoll));
    //TODO does this work?
    const option = iPoll.options[parseInt(payload.actions[0].text.text)-1]
    if(!option.voters.includes(payload.user.username)) {
      // add user to list and update vote count
      option.voters.push(payload.user.username);
      option.votes = option.votes + 1
    } else {
      //remove user from list
      option.voters = option.voters.filter(obj => obj !== payload.user.username);
      option.votes = option.votes - 1
    }
    //update Ipoll in repository
    console.log(JSON.stringify(iPoll));
    return this.pollRepository.updatePoll(iPoll);

  }

}