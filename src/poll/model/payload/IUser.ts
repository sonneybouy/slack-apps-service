export interface IUser {
  id: string,
  username: string,
  name: string,
  team_id: string
}