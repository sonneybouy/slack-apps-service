import { Types } from '../blocks/Types';

export interface IAction {
  action_id: string,
  block_id: string,
  text: IText,
  value: string,
  type: Types,
  action_ts: string
}

interface IText {
  type: Types.PLAIN_TEXT,
  text: string,
  emoji: string
}