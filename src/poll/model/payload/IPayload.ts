import { IUser } from './IUser';
import { IAction } from './IAction';

export interface IPayload {
  type: string,
  user: IUser,
  api_app_id: string,
  token: string,
  trigger_id: string,
  channel: IChannel
  response_url: string,
  actions: IAction[],
}


interface IChannel {
  id: string,
  name: string
}