import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';


@Schema()
export class Poll extends Document {

  @Prop()
  question: string;

  @Prop()
  options: any;
}

export const PollSchema = SchemaFactory.createForClass(Poll)