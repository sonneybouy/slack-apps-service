import {ObjectId} from 'mongodb'


export interface IPoll {
  id: ObjectId,
  question : string,
  options : IOption[]
}

export interface IOption {
  nomination: string,
  votes: number
  voters: string[]
}