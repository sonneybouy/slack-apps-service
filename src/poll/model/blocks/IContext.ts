import { Types } from './Types';
import { ISubSections } from './ISection';

export interface IContext extends ISubSections{
  type: Types.CONTEXT,
  elements: IElement[]

}

export interface IElement {
  type: Types,
  image_url?: string,
  alt_text?: string,
  text?: string,
  emoji?: boolean
}