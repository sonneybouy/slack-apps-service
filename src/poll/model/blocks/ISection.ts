import { Types } from './Types';

export interface ISubSections {
  type: Types
  text?: IText
  accessory?: IAccessory
}
export interface ISection extends ISubSections{
  type : Types.SECTION,
  text : IText,
  accessory?: IAccessory
}

export interface IText {
  type: Types,
  text: string
  emoji? : boolean
}

export interface IAccessory {
  type: Types.BUTTON,
  text: IText,
  value: Object
}