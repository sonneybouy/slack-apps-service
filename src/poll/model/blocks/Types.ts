export enum Types {
  SECTION = "section",
  MARKDOWN = "mrkdwn",
  BUTTON = "button",
  DIVIDER = "divider",
  CONTEXT = "context",
  IMAGE = "image",
  PLAIN_TEXT = "plain_text"
}