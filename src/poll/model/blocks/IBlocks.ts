import {  ISubSections } from './ISection';

export interface IBlocks {
  blocks: ISubSections[],
  response_type: string
}

